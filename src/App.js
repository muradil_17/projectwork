import React, {Fragment} from 'react';
import {BrowserRouter,  Route, Switch} from "react-router-dom";
import './App.css';
import FormUser from "./components/forms/formUser";
import MainPage from "./components/page/mainPage";
import Header from "./components/header/header";
import Lessons from "./components/lessons/lessons";

function App() {
  return (
      <Fragment>
          <Header/>
          <BrowserRouter>
              <Switch>
                  <Route path="/MainPage" exact component={MainPage}/>
                  <Route path="/" exact component={FormUser} />
                  <Route path="/lessons" exact component = {Lessons}/>
              </Switch>
          </BrowserRouter>
      </Fragment>
  );
}

export default App;
