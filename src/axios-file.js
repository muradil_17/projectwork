import axios from "axios";

const baseUser = axios.create({
   baseURL: "https://course-a56e5.firebaseio.com/",
});

export default baseUser;