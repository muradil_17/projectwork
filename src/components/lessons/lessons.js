import React, {Component} from 'react';
import Lesson from "./lesson";
import axios from "../../axios-file";
import Rightbar from "./rightbar";

import './lessons.css'

class Lessons extends Component {
    state={
        lessons: [],
        VideoUrl: '',
        title: '',
        text: '',
        titles: '',
        time: '',
        response: null
    };

    componentDidMount() {
        axios.get('course/lesson1.json').then(response => {
            const lessonsAdmin= [];
            lessonsAdmin.push({...response.data});
            this.setState({lessons: lessonsAdmin, lessonsAdmin: lessonsAdmin})
        })
    }

    render() {
        let lessonsAdmin = this.state.lessons.map((lessons, i) =>{
            return (
                <div>
                    <div className="User-course" key={i}>
                        <Lesson title={lessons.title}
                                video={lessons.video}
                                text={lessons.text}/>
                    </div>
                </div>
            );
        });
        return(
            <div className="User-lessons">
                {lessonsAdmin}
                <div className="right">
                    <Rightbar/>
                </div>
            </div>
        )

    }
}

export default Lessons;