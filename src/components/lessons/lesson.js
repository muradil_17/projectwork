import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faDownload} from "@fortawesome/free-solid-svg-icons";
import './lesson.css'


const  userClick = event =>{
    event.preventDefault();
    alert('Загрузка началось!')
};

const Lesson = (props) => {
    return (
        <div className="content-lessons">
            <h1 className='title'>
                {props.title}
            </h1>
            <iframe width="694" height="379" src={props.video} frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen></iframe>
            <p className="text">
                {props.text}
            </p>
            <div className="download">
                <button onClick={userClick}><FontAwesomeIcon icon={faDownload} className="icon" /></button>
                <h1>Лекция урока PDF</h1>
            </div>
        </div>
    );
};

export default Lesson;