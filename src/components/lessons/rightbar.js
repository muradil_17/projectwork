import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faDownload, faPlay} from "@fortawesome/free-solid-svg-icons";
import axios from './../../axios-file';

import './right-bar.css'

class Rightbar extends Component {
    state={
      lessons: [],
      title: '',
      time: '',
      videoURL: '',
      response: null
    };

    componentDidMount() {
        axios.get('lessons/lesson1.json').then(response =>{
            const lessonsUser =[];
            for (let key in response.data){
                lessonsUser.push({...response.data[key], id:key})
            }
            this.setState({lessons: lessonsUser, lessonsUser: lessonsUser})
        })
    }



    render() {
        let lessonUser = this.state.lessons.map((lessons, l)=>{
           return(
               <div className='right-bar'>
                   <h1>{lessons.title}</h1>
                   <p>Длительность урока: {lessons.time} минут</p>
                   <div className="video">
                       <button>
                            <FontAwesomeIcon icon={faPlay} className='icon'/>
                       </button>
                       <h4>Начать просмотр</h4>
                   </div>
                   <div className="book">
                       <button>
                            <FontAwesomeIcon icon={faDownload} className='icon'/>
                       </button>
                       <h4>Лекция урока PDF</h4>
                   </div>
               </div>
           )
        });
        return (
            <div>
                {lessonUser}
            </div>
        );
    }
}

export default Rightbar;