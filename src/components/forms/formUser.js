import React, {Component} from 'react';
import axios from '../../../src/axios-file';
import Errror from "../ModalWindow/error/errror";
import GoogleLogin from "react-google-login";
import firebase from "firebase";


import './formUser.css'
import image from './images/33236191.png'
import {StyledFirebaseAuth} from "react-firebaseui";


firebase.initializeApp({
    apiKey: 'AIzaSyCs1YS8TiC24JRvcwl1z6hnq2Clu_gN96U',
    authDomain: 'course-a56e5.firebaseapp.com'
});

class FormUser extends Component {
    state={
        login: '',
        password: '',
        user: {},
        log_in: false,
        modalShow: false,
    };

    uiConfig ={
      signInFlow: 'popup',
      signInOptions: [
          firebase.auth.GoogleAuthProvider.PROVIDER_ID
      ],
      callback: {
          signInSuccess: () => false
      }
    };


    changeHandler = event =>{
      this.setState({
          [event.target.name]: event.target.value
      });
    };

    ModalWindow = () =>{
        this.setState({modalShow: true})
    };

    checkLoginStatus = () =>{
        axios.get('/users/user1.json').then(response =>{
            if (response.data.login === this.state.login && response.data.password === this.state.password){
                this.props.history.push({
                    pathname: '/MainPage'
                });
                this.setState({
                    users: response.data.users
                })
            }else if(response.data.login !== this.state.login && response.data.password !== this.state.password){
                this.ModalWindow()
            }
        }).catch(error =>{
            this.ModalWindow()
        })
    };

    componentDidMount() {
        firebase.auth().onAuthStateChanged(user =>{
            this.setState({log_in: !!user});
            // this.props.history.push({
            //     pathname: '/MainPage'
            // });
        })
    }


    userHandler = event => {
        event.preventDefault();
        this.checkLoginStatus();
    };

    userClicked = () =>{
      this.setState({modalShow: false})
    };


    render() {
        console.log(this.state.log_in);
        return (
        <div>
            <Errror show={this.state.modalShow}>
                <h1>Ошибка</h1>
                <p>Вы ввели направильные данные или вы не авторизованы!</p>
                <div className="btn">
                    <button onClick={this.userClicked}>отмена</button>
                    <button className='ok-btn' onClick={this.userClicked}>ok</button>
                </div>
            </Errror>
            <div className="content">
                { this.state.log_in ? (
                    this.props.history.push({
                            pathname: '/MainPage'
                        })
                    ):(
                        <div className="formUser">
                            <div className="text-head">
                                <h4>Чтобы получить доступ к курсам
                                    войдите в свой личный кабинет</h4>
                            </div>
                            <div className="inputs">
                            <input type="text"
                               placeholder="Логин"
                               className="Log"
                               name="login"
                               onChange={this.changeHandler}
                            />
                            <input type="text"
                               placeholder="Пароль"
                               className="Password"
                               name="password"
                               onChange={this.changeHandler}
                            />
                            <div className="btn-user">
                                <button className="btn-ready" onClick={this.userHandler}>Начать обучение</button>
                            </div>
                        <div className="btn-google">
                            <StyledFirebaseAuth firebaseAuth={firebase.auth()}
                                                uiConfig={this.uiConfig}
                                                onClick={this.userLogin}
                            />
                        </div>
                    </div>
                </div>
                    )}
                <div className="image">
                    <img src={image} alt="img"/>
                </div>
            </div>
        </div>
        );
    }
}

export default FormUser;