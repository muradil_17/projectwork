import React, {Component} from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight} from '@fortawesome/free-solid-svg-icons';
import axios from './../../axios-file';


import './mainPage.css'



class MainPage extends Component {
    state = {
      course: [],
      title: '',
      lessons: '',
      homework: '',
      time: '',
      response: null
    };

    componentDidMount() {
        axios.get('/course.json').then(response => {
            const courseAdmin = [];
            for (let key in response.data){
                courseAdmin.push({...response.data[key], id: key})
            }
            this.setState({course: courseAdmin, courseAdmin: courseAdmin})
        })
    }

    userHandler = event =>{
        this.props.history.push("/lessons");
    };

    render() {
        let course = this.state.course.map((course, j)=>{
           return (
               <div className='result' key={j}>
                    <h1>{course.title}</h1> {/*название урока*/}
                    <p>Уроков: {course.lessons}</p>
                    <p>Длительность: {course.time} минут</p>
                    <p>Домашние задания: {course.homework}</p>
                    <div className="btn-result">
                       <button><FontAwesomeIcon icon={faArrowRight} onClick={this.userHandler} /></button>
                    </div>
                    <hr className="line"/>
               </div>
           )
        }
        );
        return (
            <div className="content">
                <div className="user-static">
                    <h1>Мой рейтинг</h1>
                    <div className="user-mission">
                        <p>Выполненных заданий: 4/32</p>
                    </div>
                    <div className="user-rating">
                        <p>Средняя оценка: 4.5</p>
                    </div>
                    <div className="user-skill">
                        <p>Навыки: Adobe Photoshop, Adobe
                            Illustrator, Типографика, Работа с
                            цветом</p>
                    </div>
                </div>
                <div className="user-course">
                    <h1>Курс “UX/UI-дизайн”</h1>
                    {course}
                    <div className="Test-user">
                        <h1>Тестирование</h1>
                        <div className="btn-begin">
                            <button>Начать</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MainPage;