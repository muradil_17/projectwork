import React, {Component} from 'react';
import "./header.css"
import Errror from "../ModalWindow/error/errror";



class Header extends Component {

    state ={
      show: false
    };

    userHandler = () => {
        if (this.props.history === ''){
            this.setState({show: true});
        }
    };

    userClicked = () =>{
        this.setState({show: false})
    };

    render() {
        return (
            <div>
                <Errror show={this.state.show}>
                    <h1>Ошибка</h1>
                    <p>Вы ввели направильные данные или вы не авторизованы!</p>
                    <div className="btn">
                        <button  onClick={this.userClicked}>отмена</button>
                        <button className='ok-btn' onClick={this.userClicked}>ok</button>
                    </div>
                </Errror>
                <header className="header">
                    <div className="buttons-header">
                        <a href="#" className="RU">РУ</a>
                        <a href="#" className="KG">КГ</a>
                        <button className="courses" onClick={this.userHandler} >Мои курсы</button>
                    </div>
                </header>
            </div>
        );
    }
}

export default Header;